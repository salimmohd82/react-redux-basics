import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";

import App from './container/App';
import store from './store';

// store.dispatch({type: "ADD", payload: 100});
// store.dispatch({type: "ADD", payload: 22});
// store.dispatch({type: "SUBSTRACT", payload: 80});
// store.dispatch({type: "NAME", payload: "Anna"});
// store.dispatch({type: "AGE", payload: 22});

render(
    <Provider store={store}>
    <App/>
</Provider>, window.document.getElementById('app'));
