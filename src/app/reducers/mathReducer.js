const initialMState = {
    result: 1,
    lastValues: []
}

const mathreducer = (state = initialMState, action) => {
    switch (action.type) {
        case "ADD":
            // state.result += action.payload
            state = {
                ...state,
                result: state.result + action.payload,
                lastValues: [
                    ...state.lastValues,
                    action.payload
                ]
            }
            break;
        case "SUBSTRACT":
            state = {
                ...state,
                result: state.result - action.payload,
                lastValues: [
                    ...state.lastValues,
                    action.payload
                ]
            }
            break;

    }
    return state;
};

export default mathreducer;
